import {Routes} from '@angular/router';
import {ListBrandsComponent} from "./list-brands/list-brands.component";
import {NgbdModalComponent} from "./edit-brand/edit-brand-component";
import {CreateBrandComponent} from "./create-brand/create-brand.component";

export const BrandRoutes: Routes = [
	{
		path: 'brands',
		children: [
			{
				path: '',
				component: ListBrandsComponent
			},
			{
				path: 'create-brand',
				component: CreateBrandComponent
			},
			{
				path: 'modal-component',
				component: NgbdModalComponent
			}
		]
	}
];
