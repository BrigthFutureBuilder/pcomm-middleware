import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CreateBrandComponent} from './create-brand/create-brand.component';
import {ListBrandsComponent} from './list-brands/list-brands.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgbdModalComponent, NgbdModalContent} from "./edit-brand/edit-brand-component";
import {BrandRoutes} from "./brand.routing";
import {RouterModule} from "@angular/router";
import {TranslateModule} from "@ngx-translate/core";


@NgModule({
  declarations:
    [
      CreateBrandComponent,
      ListBrandsComponent,
      NgbdModalComponent,
      NgbdModalContent],
  entryComponents: [NgbdModalContent],
  imports: [
    CommonModule,
    RouterModule.forChild(BrandRoutes),
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule

  ],
  exports: [
  ]

})
export class BrandModule {
}
