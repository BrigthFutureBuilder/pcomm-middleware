import {Component, OnInit} from '@angular/core';

import {debounceTime} from 'rxjs/operators';
import {Brand} from '../../model/brand';
import {BrandService} from '../../services/brand-service';
import {Router} from '@angular/router';

@Component({
  selector: 'create-brand',
  templateUrl: './create-brand.component.html'
})
export class CreateBrandComponent implements OnInit {

  brand: Brand = new Brand();
  submitted = false;

  constructor(private brandService: BrandService, private router: Router) { }

  ngOnInit() {
  }

  newBrand(): void {
    this.submitted = false;
    this.brand = new Brand();
  }

  save() {
    this.brandService.addBrand(this.brand)
      .subscribe(data => {
        debounceTime(5000),
        this.router.navigate(['/brands']);
      }, (err) => {
        console.log(err);
      });
  }

  onSubmit() {
    this.submitted = true;
    this.save();
  }
}
