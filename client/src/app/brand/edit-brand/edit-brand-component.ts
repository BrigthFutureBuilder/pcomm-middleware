import {Component, Input, OnInit} from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

import {FormBuilder, FormGroup, NgForm, Validators} from "@angular/forms";
import {BrandService} from "../../services/brand-service";
import {ActivatedRoute, Router} from "@angular/router";
import {Brand} from "../../model/brand";


@Component({
  selector: 'ngbd-modal-content',
  templateUrl: './edit-brand-component.html'
})
export class NgbdModalContent {

  brandForm: FormGroup;

  modalHeader: string;
  brand: Brand;

  constructor(public activeModal: NgbActiveModal, private brandService: BrandService, private formBuilder: FormBuilder, private route: ActivatedRoute, private router: Router) {}

  ngOnInit() {
    console.log("id:" + this.brand);
    this.brandForm= this.formBuilder.group({
      'brandName' : [this.brand.brandName, Validators.required]
    });


  }

  update() {
    this.brandService.updateBrand(this.brand)
      .subscribe(data => {
        this.activeModal.dismiss(NgbdModalComponent);
        this.router.navigate(['/brands']);
      }, (err) => {
        console.log(err);
      });
  }
  close(){
    this.activeModal.dismiss();
  }

}

@Component({
  selector: 'ngbd-modal-component',
  templateUrl: './edit-brand-component.html'
})
export class NgbdModalComponent{
  constructor(private modalService: NgbModal) {}

  open(id) {
    const modalRef = this.modalService.open(NgbdModalContent);
    modalRef.componentInstance.name = "World";

  }
}
