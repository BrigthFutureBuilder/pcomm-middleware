import {Component, Input, NgModule, OnInit} from '@angular/core';

import {Brand} from '../../model/brand';
import {BrandService} from '../../services/brand-service';
import {Router} from "@angular/router";
import {MatDialog} from "@angular/material";

import {NgbdModalContent} from "../edit-brand/edit-brand-component";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {ConfirmationDialogService} from "../../shared/confirmation-dialog/confirmation-dialog.service";
import {TranslateService} from "@ngx-translate/core";


@NgModule({
  imports: []
})
@Component({
  selector: 'list-brands',
  templateUrl: './list-brands.component.html'
})
export class ListBrandsComponent implements OnInit {

  @Input() id;
  brands: Brand[] = [];

  confirmMessage: string;

  confirmTitle: string;

  constructor(
    private brandService: BrandService,
    private router: Router,
    private modalService: NgbModal,
    private dialog: MatDialog,
    private confirmationDialogService: ConfirmationDialogService, translate: TranslateService) {
    translate.get('APP.CONFIRM.MESSAGE').subscribe((text: string) => {
      this.confirmMessage = text
    });
    translate.get('APP.CONFIRM.TITLE').subscribe((text: string) => {
      this.confirmTitle = text
    });
  }

  ngOnInit() {
    this.reloadData();
  }


  reloadData() {
    this.brandService.listBrand().subscribe(res => {
      this.brands = res;
      console.log(this.brands);
    }, err => {
      console.log(err);
    });
  }

  ngOnDelete(id) {
    this.brandService.deleteBrand(id)
      .subscribe(res => {
        this.ngOnInit();
        this.router.navigate(['/brands']);
      }, err => {
        console.log(err);
      });
  }


  public openConfirmationDialog(id) {

    this.confirmationDialogService.confirm(this.confirmTitle, this.confirmMessage)
      .then((confirmed) => this.ngOnDelete(id))
      .catch(() => console.log());

  }

  public open(brand) {
    const modalRef = this.modalService.open(NgbdModalContent)
    modalRef.componentInstance.brand= brand;
  }
}

