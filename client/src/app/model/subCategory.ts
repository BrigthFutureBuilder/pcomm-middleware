export class SubCategory {
  id: number;
  catName: string;
  catNameTr: string;
  parentId: number;
}
