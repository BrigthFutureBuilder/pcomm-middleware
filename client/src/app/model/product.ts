export class Product {
  id: number;
  productName: string;
  categoryId: number;
  brandId: number;
  imageurl:string;
}
