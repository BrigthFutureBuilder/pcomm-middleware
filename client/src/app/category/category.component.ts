import {Component, ViewChild} from '@angular/core';
import {Router} from "@angular/router";
import {TranslateService} from "@ngx-translate/core";
import {MatPaginator, MatSort, MatTableDataSource} from "@angular/material";
import {Category} from "../model/category";
import {CategoryService} from "../services/category-service";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {CategoryEditComponent} from "./category-edit/category-edit.component";
import {ConfirmationDialogService} from "../shared/confirmation-dialog/confirmation-dialog.service";

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent {
  displayedColumns: string[] = ['id', 'name', 'nameTr', 'parentId', 'actions'];

  dataSource: MatTableDataSource<Category>;

  allCategories: Category[] = [];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  confirmMessage: string;

  confirmTitle: string;

  constructor(private modalService: NgbModal, private router: Router, private categoryService: CategoryService, translate: TranslateService,
              private confirmationDialogService: ConfirmationDialogService) {

    translate.get('APP.CONFIRM.MESSAGE').subscribe((text: string) => {
      this.confirmMessage = text
    });
    translate.get('APP.CONFIRM.TITLE').subscribe((text: string) => {
      this.confirmTitle = text
    });
  }

  ngOnInit() {
    this.categoryService.getAllCategories()
      .subscribe(res => {
        this.allCategories = res;
        this.dataSource = new MatTableDataSource(this.allCategories);
      }, err => {
        console.log(err);
      });

    this.dataSource = new MatTableDataSource(this.allCategories);
  }

  ngAfterViewInit() {
    setTimeout(() => this.dataSource.paginator = this.paginator);
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  onEdit(category) {
    console.log("Edit id: " + category.id);
    const modalRef = this.modalService.open(CategoryEditComponent)
    modalRef.componentInstance.categoryModel = category;
  }

  onDeleteConfirmation(category) {
    console.log("Edit id: " + category.id);
    this.confirmationDialogService.confirm(this.confirmTitle, this.confirmMessage)
      .then((confirmed) => this.onDelete(category))
      .catch(() => console.log());
  }

  onDelete(id) {
    this.categoryService.deleteCategory(id)
      .subscribe(res => {
        this.ngOnInit();
      }, err => {
        console.log(err);
      });
  }
}
