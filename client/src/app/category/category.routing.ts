import {Routes} from '@angular/router';
import {CategoryComponent} from "./category.component";
import {CategoryAddComponent} from "./category-add/category-add.component";
import {CategoryEditComponent} from "./category-edit/category-edit.component";

export const CategoryRoutes: Routes = [
	{
		path: 'category',
		children: [
			{
				path: '',
				component: CategoryComponent
			},
			{
        path: 'category-add',
        component: CategoryAddComponent,
			},
			{
        path: 'category-edit/:id',
        component: CategoryEditComponent,
			}
		]
	}
];
