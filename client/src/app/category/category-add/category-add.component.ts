import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder} from '@angular/forms';
import {Category} from "../../model/category";
import {CategoryService} from "../../services/category-service";
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-comment-add',
  templateUrl: './category-add.component.html',
  styleUrls: ['./category-add.component.css']
})
export class CategoryAddComponent implements OnInit {

  categoryModel: Category;

  category: Category;

  subCategory: Category;

  grandCategory: Category;

  parentCategory: Category;

  categories: Category[] =  [];

  grandCategories: Category[] =  [];

  parentCategories: Category[] =  [];

  subCategories: Category[] =  [];

  parentId: number;

  currentLang: string;

  isEnglish: boolean;

  selectedCat: Category;

  constructor(private formBuilder: FormBuilder, private router: Router, private categoryService: CategoryService, translate: TranslateService) {
    this.currentLang = translate.currentLang;
    this.isEnglish = this.currentLang === 'en';
    this.grandCategory = new Category();
    this.categoryModel = new Category();
  }

  ngOnInit() {
    this.categoryModel = new Category();
    this.grandCategory = new Category();
    this.parentCategory = new Category();
    this.category = new Category();
    this.selectedCat = new Category();
    this.subCategory = new Category();
    this.categories.length = 0;
    this.subCategories.length = 0;
    this.parentCategories.length = 0;
    this.grandCategories.length = 0;
    this.parentId = undefined;
    // this.selectedCat = -1;
    this.grandCategories.push(this.selectedCat);
    this.categoryService.getCategories()
      .subscribe(res => {
        this.grandCategories = res;
      }, err => {
        console.log(err);
      });

  }

  onSave(category) {
    if(this.parentId){
      category.parentId = this.parentId;
      delete category.id;
    }
    this.categoryService.addCategory(category)
      .subscribe(data => {
          if(data){
            this.ngOnInit();
          }
          // this.router.navigate(['/category']);
        }, (err) => {
                console.log(err);
    });
  }

  onChange(parentId, mode) {
    this.parentId = parentId;
    this.categoryService.getSubCategories(parentId)
      .subscribe(res => {
        if(mode == "parentCategory"){
          this.parentCategories = res;
          if(res.length == 0){
            this.categories = res;
            this.subCategories = res;
            this.parentCategory = new Category();
            this.category = new Category();
            this.subCategory = new Category();
          }
        }else if(mode === "category"){
          this.categories = res;
          if(res.length == 0){
            this.subCategories = res;
            this.category = new Category();
            this.subCategory = new Category();
          }
        }else if(mode === "subCategory"){
          this.subCategories = res;
          if(res.length == 0){
            this.subCategory = new Category();
          }
        }
      }, err => {
        console.log(err);
      });
  }

  onChangeSubCat(parentId){
    this.parentId = parentId;
  }
  onCancel() {
    this.router.navigate(['/comments']);
  }

}
