import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CategoryComponent} from './category.component';
import {CategoryAddComponent} from './category-add/category-add.component';
import {CategoryEditComponent} from './category-edit/category-edit.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from "@angular/router";
import {CategoryRoutes} from "./category.routing";
import {TranslateModule} from "@ngx-translate/core";
import {MaterialModule} from "../shared/material.module";
import {BrowserModule} from "@angular/platform-browser";
import {MatSortModule, MatTableModule} from "@angular/material";

@NgModule({
  declarations: [CategoryComponent, CategoryAddComponent, CategoryEditComponent],
  entryComponents: [CategoryComponent],
  imports: [
    BrowserModule,
    CommonModule,
    RouterModule.forChild(CategoryRoutes),
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    MaterialModule,
    MatSortModule,
    MatTableModule
  ],
  exports: [
    CategoryComponent
  ]

})
export class CategoryModule { }
