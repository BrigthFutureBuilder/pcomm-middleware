import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {FormBuilder, FormGroup} from "@angular/forms";
import {Category} from "../../model/category";
import {CategoryService} from "../../services/category-service";
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-category-edit',
  templateUrl: './category-edit.component.html',
  styleUrls: ['./category-edit.component.css']
})
export class CategoryEditComponent implements OnInit {

  category: Category;

  categoryModel: Category;

  categories: Category[] = [];

  parentId: number;

  constructor(public activeModal: NgbActiveModal, private router: Router, private route: ActivatedRoute, private categoryService: CategoryService) { }

  ngOnInit() {
    this.category = this.categoryModel;
    this.parentId = this.categoryModel.parentId;
    this.categoryService.getAllParentCategories(this.categoryModel.parentId)
      .subscribe(data => {
        if(data){
          this.categories = data;
          this.category = this.categories.filter(category => category.id === this.categoryModel.parentId)[0];

        }
      }, (err) => {
        console.log(err);
      });

    console.log(this.categoryModel);

  }

  onChange(category){
    this.parentId = category.id;
    this.category = category;
  }

  onUpdate(category){
    if(this.parentId){
      category.parentId = this.parentId;
    }
    this.categoryService.updateCategory(category)
      .subscribe(data => {
        if(data){
          this.ngOnInit();
        }
        // this.router.navigate(['/category']);
      }, (err) => {
        console.log(err);
      });
  }
}
