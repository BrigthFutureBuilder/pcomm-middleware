import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError, tap} from 'rxjs/operators';
import {Comment} from "../model/comment";
import {ToastrService} from "../shared/services/toastr.service";
import {environment} from "../../environments/environment";

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class CommentService {

  constructor(private http: HttpClient,
              private toasterService: ToastrService) {
  }

  getComments(): Observable<Comment[]> {
    const url = `${environment.apiCommentUrl}/list/1`;
    return this.http.get<Comment[]>(url)
      .pipe(
        tap(comments => console.log('Fetch comment')),
        catchError(this.handleError('error', []))
      );
  }

  addComment(comment): Observable<Comment> {
    const url = `${environment.apiCommentUrl}/add`;
    console.log("comment: " + comment)
    return this.http.post<Comment>(url, comment, httpOptions).pipe(
      tap((comment: Comment) =>
        this.toasterService.success('Success', 'Comment added')),
      catchError(this.handleError<Comment>('error'))
    );
  }

  updateComment(comment, id): Observable<any> {
    const url = `${environment.apiCommentUrl}/update`;
    comment.id = id;
    return this.http.put(url, comment, httpOptions).pipe(
      tap(_ =>  this.toasterService.success('Success', 'Comment updated')),
      catchError(this.handleError<any>('error'))
    );
  }

  deleteComment(id): Observable<Comment> {
    const url = `${environment.apiCommentUrl}/delete/${id}`;

    return this.http.delete<Comment>(url, httpOptions).pipe(
      tap(_ => this.toasterService.success('Success', 'Comment deleted')),
      catchError(this.handleError<Comment>('error'))
    );
  }

  getComment(id: number): Observable<Comment> {
    const url = `${environment.apiCommentUrl}/${id}`;
    return this.http.get<Comment>(url).pipe(
      tap(_ => console.log(`fetched Comment id=${id}`)),
      catchError(this.handleError<Comment>(`Error`))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
      this.toasterService.error('Error', error);
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
