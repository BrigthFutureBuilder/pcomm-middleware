import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

import { environment } from '../../environments/environment';
import { Brand} from '../model/brand';
import {catchError, tap} from "rxjs/operators";
import {Observable, of} from "rxjs";

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};
@Injectable()
export class BrandService {
  constructor(private http: HttpClient) { }


  listBrand() {
    return this.http.get<Brand[]>(`${environment.apiBrandUrl}/list`);
  }

  getBrandById(id: number) {
    return this.http.get<Brand>(`${environment.apiBrandUrl}/read/${id}`);
  }


  addBrand(brand: Brand) {

    return this.http.post(`${environment.apiBrandUrl}/add`, brand);

  }
  updateBrand(brand: Brand) {

    return this.http.put(`${environment.apiBrandUrl}/update`, brand);

  }

  deleteBrand(id: number) {

    return this.http.delete<Brand>(`${environment.apiBrandUrl}/delete/${id}`, httpOptions).pipe(
      tap(_ => console.log(`deleted comment id=${id}`))
    );

    }

  getBrandsByName (searchText: String): Observable<Brand[]> {
    const url = `${environment.apiBrandUrl}/brandName/`+ searchText;
    return this.http.get<Brand[]>(url)
      .pipe(
        tap(brands => console.log('Fetch Brand')),
        catchError(this.handleError('Brands', []))
      );
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
