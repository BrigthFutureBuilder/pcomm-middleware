import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

import { environment } from '../../environments/environment';
import { Product } from '../model/product';
import {catchError, tap} from "rxjs/operators";
import {Observable, of} from "rxjs";

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};
@Injectable()
export class ProductService {
  constructor(private http: HttpClient) { }


  listProduct() {
    return this.http.get<Product[]>(`${environment.apiUrl}/list`);
  }

  getById(id: number) {
    return this.http.get<Product>(`${environment.apiUrl}/${id}`);
  }


  addProduct(product: Product) {

    return this.http.post(`${environment.apiUrl}/add`, product);

  }
  updateProduct(product: Product) {

    return this.http.put(`${environment.apiUrl}/update`, product);

  }

  deleteProduct(id: number) {

    return this.http.delete<Product>(`${environment.apiUrl}/delete/${id}`, httpOptions).pipe(
      tap(_ => console.log(`deleted comment id=${id}`))
    );

    }

  getProductsByName (searchText: String): Observable<Product[]> {
    const url = `${environment.apiUrl}/productName/`+ searchText;
    return this.http.get<Product[]>(url)
      .pipe(
        tap(products => console.log('Fetch Product')),
        catchError(this.handleError('Products', []))
      );
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
