import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError, tap} from 'rxjs/operators';
import {ToastrService} from "../shared/services/toastr.service";
import {environment} from "../../environments/environment";
import {Category} from "../model/category";
import {SubCategory} from "../model/subCategory";

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class CategoryService{

  constructor(private http: HttpClient,
              private toasterService: ToastrService) {
  }

  getCategories(): Observable<Category[]> {
    const url = `${environment.apiCategoryUrl}/parentCategories`;
    return this.http.get<Category[]>(url)
      .pipe(
        tap(comments => console.log('Fetch comment')),
        catchError(this.handleError('error', []))
      );
  }

  getAllCategories(): Observable<Category[]> {
    const url = `${environment.apiCategoryUrl}/allCategories`;
    return this.http.get<Category[]>(url)
      .pipe(
        tap(comments => console.log('Fetch comment')),
        catchError(this.handleError('error', []))
      );
  }

  getAllParentCategories(categoryId): Observable<Category[]> {
    let url = `${environment.apiCategoryUrl}/allParentCategories/${categoryId}`;
    if(categoryId === 0){
      url = `${environment.apiCategoryUrl}/parentCategories`;
    }
    return this.http.get<Category[]>(url)
      .pipe(
        tap(comments => console.log('Fetch comment')),
        catchError(this.handleError('error', []))
      );
  }

  getSubCategories(parentId): Observable<SubCategory[]> {
    const url = `${environment.apiCategoryUrl}/subCategories/${parentId}`;
    if(parentId){
      return this.http.get<Category[]>(url)
        .pipe(
          catchError(this.handleError('error', []))
        );
    }else{
        return of([]);
    }
  }

  addCategory(category): Observable<Category> {
    const url = `${environment.apiCategoryUrl}/add`;
    return this.http.post<Category>(url, category, httpOptions).pipe(
      tap((category: Category) =>
        this.toasterService.success('Success', 'Category added')),
      catchError(this.handleError<Category>('error'))
    );
  }

  updateCategory(category): Observable<any> {
    const url = `${environment.apiCategoryUrl}/update`;
    return this.http.put(url, category, httpOptions).pipe(
      tap(_ =>  this.toasterService.success('Success', 'Category updated')),
      catchError(this.handleError<any>('error'))
    );
  }

  deleteCategory(id): Observable<Category> {
    const url = `${environment.apiCategoryUrl}/delete/${id}`;

    return this.http.delete<Category>(url, httpOptions).pipe(
      tap(_ => this.toasterService.success('Success', 'Category deleted')),
      catchError(this.handleError<Category>('error'))
    );
  }

  // getComment(id: number): Observable<Comment> {
  //   const url = `${environment.apiCommentUrl}/${id}`;
  //   return this.http.get<Comment>(url).pipe(
  //     tap(_ => console.log(`fetched Comment id=${id}`)),
  //     catchError(this.handleError<Comment>(`Error`))
  //   );
  // }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error.error.message); // log to console instead
      this.toasterService.error('Error', error.error.message);
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
