import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {AppComponent} from './app.component';
import {CommentModule} from "./comment/comment.module";
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule} from '@angular/forms';
import {SharedModule} from "./shared/shared.module";
import {ProductModule} from "./product/product.module";
import {TranslateLoader, TranslateModule, TranslateService} from "@ngx-translate/core";
import {TranslateHttpLoader} from "@ngx-translate/http-loader";
import {CategoryModule} from "./category/category.module";
import {ProductService} from "./services/product-service";
import {BrandModule} from "./brand/brand.module";
import {BrandService} from "./services/brand-service";

export function HttpLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient);
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CommentModule,
    CategoryModule,
    HttpClientModule,
    NgbModule,
    FormsModule,
    SharedModule,
    ProductModule,
    BrandModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  providers: [
    TranslateService,
    ProductService,
    BrandService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
