import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {catchError, debounceTime, distinctUntilChanged, map, startWith, switchMap, tap} from 'rxjs/operators';
import {Router} from "@angular/router";
import {CommentService} from "../../services/comment-service";
import {Comment} from "../../model/comment";
import {Product} from "../../model/product";
import {FormBuilder, FormControl} from "@angular/forms";
import {UtilsHelperService} from "../../services/utils-helper.service";
import {ToastrService} from "../services/toastr.service";
import {of} from "rxjs/index";
import {ProductService} from "../../services/product-service";

@Component({
  selector: 'search',
  templateUrl: './search.component.html',
  styles: [`.form-control {
    width: 500px;
  }`],
  styleUrls: ['./search.component.scss'],
  animations: [UtilsHelperService.fadeInOut()]
})
export class SearchComponent implements OnInit {

  netImage: any = "../assets/img/pcomm_logo.png";
  productImage: any = "../assets/img/default_product.png";
  loading = false;
  options: any;
  public model: any;

  filteredProducts: any;
  products: Product[] = [];

  searchFormControl = new FormControl();

  constructor(private router: Router, private productService: ProductService, private formBuilder: FormBuilder,
              private toasterService: ToastrService) {
  }

  ngOnInit(): void {
    this.searchFormControl.valueChanges.pipe(
      startWith(null),
      map(value => {
        console.log(value);
        this.search(value)
      }))
      .subscribe(res => this.filteredProducts = res);

    this.options = {
      dots: false,
      responsive: {
        '0': {items: 1, margin: 5},
        '430': {items: 2, margin: 5},
        '550': {items: 3, margin: 5},
        '670': {items: 4, margin: 5},
        '790': {items: 4, margin: 5},
        '810': {items: 4, margin: 5}
      },
      items: 6,
      autoWidth: true,
      autoplay: false,
      loop: false,
      autoplayTimeout: 3000,
      lazyLoad: true,
      nav: true,
    };
  }


  search(val: string) {
    this.loading = true;
    this.productService.getProductsByName(val)
      .subscribe(res => {
        this.loading = false;
        this.products = res;
        console.log(this.products);
      }),
      catchError(this.handleError<Comment>(`Error`))
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
      this.toasterService.error('Error', error);
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}
