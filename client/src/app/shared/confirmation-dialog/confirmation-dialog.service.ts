import {Injectable} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ConfirmationDialogComponent} from './confirmation-dialog.component';
import {HttpClient} from "@angular/common/http";
import {TranslateService} from "@ngx-translate/core";

@Injectable()
export class ConfirmationDialogService {

  submitType: string = 'Ok';
  cancel: string = 'Cancel';

  constructor(private modalService: NgbModal,private http: HttpClient,translate: TranslateService) {
    translate.get('APP.OK').subscribe((text:string) => {this.submitType = text});
    translate.get('APP.CANCEL').subscribe((text:string) => {this.cancel = text});
  }

  public confirm(
    title: string,
    message: string,
    btnOkText: string = this.submitType,
    btnCancelText: string = this.cancel,
    dialogSize: 'sm'|'lg' = 'sm'): Promise<boolean> {
    const modalRef = this.modalService.open(ConfirmationDialogComponent, { size: dialogSize });
    modalRef.componentInstance.title = title;
    modalRef.componentInstance.message = message;
    modalRef.componentInstance.btnOkText = btnOkText;
    modalRef.componentInstance.btnCancelText = btnCancelText;

    return modalRef.result;
  }

}
