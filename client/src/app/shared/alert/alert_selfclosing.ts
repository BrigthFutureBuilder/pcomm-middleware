import {Component, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {debounceTime} from 'rxjs/operators';

@Component({
  selector: 'ngbd-alert-selfclosing',
  templateUrl: './alert_selfclosing.html'
})
export class NgbdAlertSelfclosing implements OnInit {
  private _success = new Subject<string>();

  staticAlertClosed = false;
  successMessage: string;

  ngOnInit(): void {

    setTimeout(() => this.staticAlertClosed = true, 20000);
    if(this._success == undefined){
      this._success = new Subject<string>();
    }
    this._success.subscribe((message) => this.successMessage = message);
    this._success.pipe(
      debounceTime(5000)
    ).subscribe(() => this.successMessage = null);
  }

  public deleteSuccessMessage() {
    this.ngOnInit();
    this._success.next(`Comment is deleted`);
  }

  public addSuccessMessage() {
    this.ngOnInit();
    this._success.next(`Comment is added`);
  }
}
