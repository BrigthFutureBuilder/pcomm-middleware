import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MaterialModule} from "./material.module";
import {SearchComponent} from "./search/search.component";
import {ReactiveFormsModule} from "@angular/forms";
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {OwlModule} from "ngx-owl-carousel";
import {NgxContentLoadingModule} from "ngx-content-loading";
import {ProductLoaderComponent} from "./productloader/product-loader.component";
import {ConfirmationDialogService} from "./confirmation-dialog/confirmation-dialog.service";
import {ConfirmationDialogComponent} from "./confirmation-dialog/confirmation-dialog.component";
import {FooterComponent} from "./footer/footer.component";
import {TranslateModule} from "@ngx-translate/core";

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MaterialModule,
    BrowserAnimationsModule,
    OwlModule,
    NgxContentLoadingModule,
    TranslateModule
  ],
  declarations: [
    SearchComponent,
    ProductLoaderComponent,
    ConfirmationDialogComponent,
    FooterComponent
  ],
  exports: [
    MaterialModule,
    SearchComponent,
    ProductLoaderComponent,
    BrowserAnimationsModule,
    OwlModule,
    NgxContentLoadingModule,
    FooterComponent
  ],
  providers: [ ConfirmationDialogService ],
  entryComponents: [ ConfirmationDialogComponent ],
})

export class SharedModule {
}
