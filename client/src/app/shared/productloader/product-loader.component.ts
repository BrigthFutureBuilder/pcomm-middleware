import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "app-product-loader",
  templateUrl: "./product-loader.component.html",
  styleUrls: ["./product-loader.component.scss"]
})
export class ProductLoaderComponent implements OnInit {
  @Input() loop: number;
  @Input() height: number;

  constructor() {}

  ngOnInit() {}

  arrayOne(n: number): any[] {
    return Array(n);
  }
}
