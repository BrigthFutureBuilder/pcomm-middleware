import {Component, OnInit} from "@angular/core";
import {LangChangeEvent, TranslateService} from "@ngx-translate/core";

@Component({
  selector: "app-footer",
  templateUrl: "./footer.component.html",
  styleUrls: ["./footer.component.scss"]
})
export class FooterComponent implements OnInit {

  currentLang: string;

  constructor(public translate: TranslateService) {

    translate.addLangs(['en', 'tr', 'fr']);
    translate.setDefaultLang('en');

    let selectedLang: string;
    selectedLang = localStorage.getItem("lang");
    if(selectedLang){
      translate.use(selectedLang.match(/en|tr|fr/) ? selectedLang : 'en');
    }else{
      const browserLang = translate.getBrowserLang();
      translate.use(browserLang.match(/en|tr|fr/) ? browserLang : 'en');
    }

    this.translate.onLangChange
      .subscribe((event: LangChangeEvent) => {
        this.translate.setDefaultLang("en");
        if(event.lang != localStorage.getItem("lang")){
          localStorage.setItem("lang", event.lang);
          window.location.reload();
        }
      });

  }
  ngOnInit(): void {
  }

}
