import {Routes} from '@angular/router';
import {ListProductComponent} from "./list-products/list-products.component";
import {NgbdModalComponent} from "./edit-product/edit-product-component";
import {CreateProductComponent} from "./create-product/create-product.component";

export const ProductRoutes: Routes = [
	{
		path: 'products',
		children: [
			{
				path: '',
				component: ListProductComponent
			},
			{
				path: 'create-product',
				component: CreateProductComponent
			},
			{
				path: 'modal-component',
				component: NgbdModalComponent
			}
		]
	}
];
