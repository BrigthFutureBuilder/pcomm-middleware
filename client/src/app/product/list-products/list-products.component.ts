import {Component, Input, NgModule, OnInit} from '@angular/core';

import {Product} from '../../model/product';
import {ProductService} from '../../services/product-service';
import {Router} from "@angular/router";
import {MatDialog} from "@angular/material";

import {NgbdModalContent} from "../edit-product/edit-product-component";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {ConfirmationDialogService} from "../../shared/confirmation-dialog/confirmation-dialog.service";
import {TranslateService} from "@ngx-translate/core";


@NgModule({
  imports: []
})
@Component({
  selector: 'list-products',
  templateUrl: './list-products.component.html'
})
export class ListProductComponent implements OnInit {

  @Input() id;
  products: Product[] = [];

  confirmMessage: string;

  confirmTitle: string;

  constructor(
    private productService: ProductService,
    private router: Router,
    private modalService: NgbModal,
    private dialog: MatDialog,
    private confirmationDialogService: ConfirmationDialogService, translate: TranslateService) {
    translate.get('APP.CONFIRM.MESSAGE').subscribe((text: string) => {
      this.confirmMessage = text
    });
    translate.get('APP.CONFIRM.TITLE').subscribe((text: string) => {
      this.confirmTitle = text
    });
  }

  ngOnInit() {
    this.reloadData();
  }


  reloadData() {
    this.productService.listProduct().subscribe(res => {
      this.products = res;
      console.log(this.products);
    }, err => {
      console.log(err);
    });
  }

  ngOnDelete(id) {
    this.productService.deleteProduct(id)
      .subscribe(res => {
        this.ngOnInit();
        this.router.navigate(['/products']);
      }, err => {
        console.log(err);
      });
  }


  public openConfirmationDialog(id) {

    this.confirmationDialogService.confirm(this.confirmTitle, this.confirmMessage)
      .then((confirmed) => this.ngOnDelete(id))
      .catch(() => console.log());

  }

  public open(product) {
    const modalRef = this.modalService.open(NgbdModalContent)
    modalRef.componentInstance.product = product;
  }
}

