import {Component, OnInit} from '@angular/core';

import {debounceTime} from 'rxjs/operators';
import {Product} from '../../model/product';
import {ProductService} from '../../services/product-service';
import {Router} from '@angular/router';

@Component({
  selector: 'create-product',
  templateUrl: './create-product.component.html'
})
export class CreateProductComponent implements OnInit {

  product: Product = new Product();
  submitted = false;

  constructor(private productService: ProductService, private router: Router) { }

  ngOnInit() {
  }

  newProduct(): void {
    this.submitted = false;
    this.product = new Product();
  }

  save() {
    this.productService.addProduct(this.product)
      .subscribe(data => {
        debounceTime(5000),
        this.router.navigate(['/products']);
      }, (err) => {
        console.log(err);
      });
  }

  onSubmit() {
    this.submitted = true;
    this.save();
  }
}
