import {Component, Input, OnInit} from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

import {FormBuilder, FormGroup, NgForm, Validators} from "@angular/forms";
import {ProductService} from "../../services/product-service";
import {ActivatedRoute, Router} from "@angular/router";
import {Product} from "../../model/product";


@Component({
  selector: 'ngbd-modal-content',
  templateUrl: './edit-product-component.html'
})
export class NgbdModalContent {

  productForm: FormGroup;

  modalHeader: string;
  product: Product;

  constructor(public activeModal: NgbActiveModal, private productService: ProductService, private formBuilder: FormBuilder, private route: ActivatedRoute, private router: Router) {}

  ngOnInit() {
    console.log("id:" + this.product);
    //this.getProduct(this.id);
    this.productForm = this.formBuilder.group({
      'productName' : [this.product.productName, Validators.required],
      'brandId' : [this.product.brandId, Validators.required],
      'categoryId' : [this.product.categoryId, Validators.required]
    });


  }

  update() {
    this.productService.updateProduct(this.product)
      .subscribe(data => {
        this.activeModal.dismiss(NgbdModalComponent);
        this.router.navigate(['/products']);
      }, (err) => {
        console.log(err);
      });
  }
  close(){
    this.activeModal.dismiss();
  }

}

@Component({
  selector: 'ngbd-modal-component',
  templateUrl: './edit-product-component.html'
})
export class NgbdModalComponent{
  constructor(private modalService: NgbModal) {}

  open(id) {
    const modalRef = this.modalService.open(NgbdModalContent);
    modalRef.componentInstance.name = "World";

  }
}
