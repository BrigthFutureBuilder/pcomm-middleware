import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CreateProductComponent} from './create-product/create-product.component';
import {ListProductComponent} from './list-products/list-products.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgbdModalComponent, NgbdModalContent} from "./edit-product/edit-product-component";
import {ProductRoutes} from "./product.routing";
import {RouterModule} from "@angular/router";
import {TranslateModule} from "@ngx-translate/core";


@NgModule({
  declarations:
    [
      CreateProductComponent,
      ListProductComponent,
      NgbdModalComponent,
      NgbdModalContent],
  entryComponents: [NgbdModalContent],
  imports: [
    CommonModule,
    RouterModule.forChild(ProductRoutes),
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule

  ],
  exports: [
  ]

})
export class ProductModule {
}
