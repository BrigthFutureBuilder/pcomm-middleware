import {Component, OnInit} from '@angular/core';
import {Comment} from "../model/comment";
import {Router} from "@angular/router";
import {CommentService} from "../services/comment-service";
import {ConfirmationDialogService} from "../shared/confirmation-dialog/confirmation-dialog.service";
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.css']
})
export class CommentComponent implements OnInit {

  comments: Comment[] = [];

  showDeleted: Boolean = false;

  submitType: string = 'Save';

  selectedRow: number;

  commentModel = Comment;

  confirmMessage: string;

  confirmTitle: string;

  constructor(private commentService: CommentService, private router: Router, private confirmationDialogService: ConfirmationDialogService, translate: TranslateService) {
    translate.get('APP.CONFIRM.MESSAGE').subscribe((text: string) => {
      this.confirmMessage = text
    });
    translate.get('APP.CONFIRM.TITLE').subscribe((text: string) => {
      this.confirmTitle = text
    });
  }

  ngOnInit() {
    this.commentService.getComments()
      .subscribe(res => {
        this.comments = res;
        console.log(this.comments);
      }, err => {
        console.log(err);
      });
  }

  public openConfirmationDialog(id) {
    this.confirmationDialogService.confirm(this.confirmTitle, this.confirmMessage)
      .then((confirmed) => this.onDelete(id))
      .catch(() => console.log());
  }

  onNew() {
    this.commentModel = Comment;

    this.submitType = 'Save';
  }

  onEdit(index: number) {
    this.selectedRow = index;
    this.commentModel = Comment;
    this.submitType = 'Update';
  }

  onDelete(id: number) {
    this.commentService.deleteComment(id)
      .subscribe(res => {
        this.showDeleted = true;
        this.ngOnInit();
        this.router.navigate(['/comments']);
        console.log(this.comments);
      }, err => {
        console.log(err);
      });

  }

}
