import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, NgForm, Validators} from '@angular/forms';
import {CommentService} from "../../services/comment-service";

@Component({
  selector: 'app-comment-add',
  templateUrl: './comment-add.component.html',
  styleUrls: ['./comment-add.component.css']
})
export class CommentAddComponent implements OnInit {

  commentForm: FormGroup;

  constructor(private formBuilder: FormBuilder, private router: Router, private commentService: CommentService) { }

  ngOnInit() {
    this.commentForm = this.formBuilder.group({
      'commentText' : [null, Validators.required],
      'productId' : [null, Validators.required]
    });
  }

  onSave(form:NgForm) {
    this.commentService.addComment(form)
      .subscribe(data => {
          this.router.navigate(['/comments']);
        }, (err) => {
                console.log(err);
    });
  }

  onCancel() {
    this.router.navigate(['/comments']);
  }

}
