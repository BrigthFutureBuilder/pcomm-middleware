import {Routes} from '@angular/router';
import {CommentComponent} from "./comment.component";
import {CommentAddComponent} from "./comment-add/comment-add.component";
import {CommentEditComponent} from "./comment-edit/comment-edit.component";

export const CommentRoutes: Routes = [
	{
		path: 'comments',
		children: [
			{
				path: '',
				component: CommentComponent
			},
			{
        path: 'comment-add',
        component: CommentAddComponent,
			},
			{
        path: 'comment-edit/:id',
        component: CommentEditComponent,
			}
		]
	}
];
