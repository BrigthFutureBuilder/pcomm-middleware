import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {FormBuilder, FormGroup, NgForm, Validators} from "@angular/forms";
import {CommentService} from "../../services/comment-service";

@Component({
  selector: 'app-comment-edit',
  templateUrl: './comment-edit.component.html',
  styleUrls: ['./comment-edit.component.css']
})
export class CommentEditComponent implements OnInit {

  id: number;

  commentForm: FormGroup;

  constructor(private router: Router, private route: ActivatedRoute, private commentService: CommentService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.getComment(this.route.snapshot.params['id']);
    this.commentForm = this.formBuilder.group({
      'commentText' : [null, Validators.required],
      'productId' : [null, Validators.required]
    });
  }

  getComment(id) {
    this.commentService.getComment(id).subscribe(data => {
      this.id = data.id;
      this.commentForm.setValue({
        commentText: data.commentText,
        productId: data.productId
      });
    });
  }

  onSave(form:NgForm) {
    this.commentService.updateComment(form, this.id)
      .subscribe(data => {
        this.router.navigate(['/comments']);
      }, (err) => {
        console.log(err);
      });
  }
  onCancel() {
    this.router.navigate(['/comments']);
  }

}
