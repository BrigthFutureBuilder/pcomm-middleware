import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CommentComponent} from './comment.component';
import {CommentAddComponent} from './comment-add/comment-add.component';
import {CommentEditComponent} from './comment-edit/comment-edit.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from "@angular/router";
import {CommentRoutes} from "./comment.routing";
import {TranslateModule} from "@ngx-translate/core";

@NgModule({
  declarations: [CommentComponent, CommentAddComponent, CommentEditComponent],
  entryComponents: [CommentComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(CommentRoutes),
    FormsModule,
    ReactiveFormsModule,
    TranslateModule
  ],
  exports: [
    CommentComponent
  ]

})
export class CommentModule { }
